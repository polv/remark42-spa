FROM patarapolw/remark42:dev
EXPOSE 8080
CMD ["/srv/remark42", "server"]